import React, { useState } from 'react';
import CsvViewer from "react-csv-viewer";
import {app} from "../firebaseConfig"

export default function Form() {
  const onUpload = (e) =>{
    const file = e.target.files[0]
    const storageRef = app.storage().ref()
    const fileRef = storageRef.child(file.name)
    fileRef.put(file).then((snapshot)=>{
      console.log("file uploaded")
       fileRef.getDownloadURL().then((url) =>{
        console.log(url)
        fetch("https://nostalgic-joliot-6fa534.netlify.app/.netlify/functions/csv", {
        method: 'POST',
        body: JSON.stringify({"url": url}),
        headers: { 'content-Type': 'application/json'},
        }).then(data=>data.json()).then(items =>{
        console.log(items)
    })
      })
    })
  }
    return (
        <div className="bg-gray-50">
  {/* <div>
  <h2>React CSV File Uploader and Viewer</h2>
      <p>Choose a csv file to view its contents</p>
      <CsvViewer />
      <a class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-indigo-50" type="submit"  onClick={onUpload}>
        Submit
      </a>
  </div> */}
  <input type="file" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
   onChange={onUpload}/>

</div>
    )
  }
  