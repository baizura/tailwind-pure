import React, { useEffect } from 'react';


export default function Team() {
  const { useState } = React;
  const [data, setData] = useState([]);
  var sqlArray = []
  useEffect(() =>{
    fetch("https://nostalgic-joliot-6fa534.netlify.app/.netlify/functions/api", {
      method: 'GET',
      headers: { 'content-Type': 'application/json'},
    }).then(data=>data.json()).then(items =>{
      setData(items)
      sqlArray = items 
      console.log(items)
    })
    
    // .then(data =>
    //      //data.json()
    //   fetch("https://60178915cb006f11a989b791--nostalgic-joliot-6fa534.netlify.app/.netlify/functions/api", {
    //     method: 'GET',
    //     headers: { 'content-Type': 'application/json'},
    //   }).then(data => data.json)
    //   .then(items => {
    //     setData(items)
    //     sqlArray = items
    //     console.log(sqlArray)
    //     console.log(data)
    //   }) 
    // )

}, [])
  
    return (
        // <!-- This example requires Tailwind CSS v2.0+ -->
<div className="flex flex-col">
  <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
      <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
        <table className="min-w-full divide-y divide-gray-200">
          <thead className="bg-gray-50">
            <tr>
              <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Name
              </th>
              <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Email
              </th>
              <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Role
              </th>
              <th scope="col" className="relative px-6 py-3">
                <span className="sr-only">Edit</span>
              </th>
            </tr>
          </thead>
          
          <tbody className="bg-white divide-y divide-gray-200">
          <tr>
            <td className="px-6 py-4 whitespace-nowrap">
              <div className="flex items-center">
                <div className="ml-4">
                  <div className="text-sm font-medium text-gray-900">
                  {data.length > 0? data[0].Name: ""}
                  </div>
                </div>
              </div>
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
            <div className="flex items-center">
              <div className="text-sm text-gray-900">{data.length > 0? data[0].Email: ""}</div>
              </div>
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
            <div className="flex items-center">
              <div className="text-sm text-gray-900">{data.length > 0? data[0].Role: ""}</div>
              </div>
            </td>
            <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
              <a className="text-indigo-600 hover:text-indigo-900">Edit</a>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
      {/* <div>
        {data.length > 0? data[0].Email: ""}
      </div> */}
    </div>
  </div>
</div>
        
    )
  }
  