import firebase from 'firebase/app'
import "firebase/storage"


const firebaseConfig = {
    apiKey: "AIzaSyA4aQUi3oLbfdkR3wWAepJu67MSQbFoJR4",
    authDomain: "dashflow-ae910.firebaseapp.com",
    databaseURL: "https://dashflow-ae910.firebaseio.com",
    projectId: "dashflow-ae910",
    storageBucket: "dashflow-ae910.appspot.com",
    messagingSenderId: "308142498984",
    appId: "1:308142498984:web:d68a18a34b2c6984ba8e9d",
    measurementId: "G-20KPEGKG3C"
  };

export const app = firebase.initializeApp(firebaseConfig)